//
//  sunMyScene.m
//  How I shot their planes
//
//  Created by El Desperado on 2/4/14.
//  Copyright (c) 2014 El Desperado. All rights reserved.
//

#import "sunMyScene.h"

@implementation sunMyScene

#pragma mark - Init and preload resource
-(void)didMoveToView:(SKView *)view {
    if (!_isContentLoaded) {
        [self preloadTextureAtlas];
        [self preloadGUI];
    }
}
-(void)preloadTextureAtlas {
    // loading explosions
    SKTextureAtlas *explosionAtlas = [SKTextureAtlas atlasNamed:@"EXPLOSION"];
    NSArray *textureNames = [explosionAtlas textureNames];
    _explosionTextures = [NSMutableArray new];
    for (NSString *name in textureNames) {
        SKTexture *texture = [SKTexture textureWithImageNamed:name];
        [_explosionTextures addObject:texture];
    }
    [explosionAtlas preloadWithCompletionHandler:^{
        _isContentLoaded = YES;
    }];
    
    // loading enemies
    SKTextureAtlas *enemiesAtlas = [SKTextureAtlas atlasNamed:@"Enemies"];
    NSArray *enemiesTextureNames = [enemiesAtlas textureNames];
    _enemiesTextures = [NSMutableArray new];
    for (NSString *name in enemiesTextureNames) {
        SKTexture *texture = [SKTexture textureWithImageNamed:name];
        [_enemiesTextures addObject:texture];
    }
    [enemiesAtlas preloadWithCompletionHandler:^{
        _isContentLoaded = YES;
    }];
    
    // loading meteors
    SKTextureAtlas *meteorAtlas = [SKTextureAtlas atlasNamed:@"Meteor"];
    NSArray *meteorTextureNames = [meteorAtlas textureNames];
    _meteorTextures = [NSMutableArray new];
    for (NSString *name in meteorTextureNames) {
        SKTexture *texture = [SKTexture textureWithImageNamed:name];
        [_meteorTextures addObject:texture];
    }
    [meteorAtlas preloadWithCompletionHandler:^{
        _isContentLoaded = YES;
    }];
}
-(void)preloadGUI {
    NSString *endTitle = @"Game Over";
    
    _endGameTitle = [SKLabelNode labelNodeWithFontNamed:@"KenVector Future"];
    _endGameTitle.text = endTitle;
    _endGameTitle.zPosition = 3;
    _endGameTitle.fontSize = 30;
    _endGameTitle.position = CGPointMake(screenWidth / 2, screenHeight / 2 + 20);
    
    _replayBTN = [UIButton buttonWithType:UIButtonTypeCustom];
    _replayBTN.tag = 999;
    UIImage *replayBTNImage = [UIImage imageNamed:@"buttonYellow"];
    [_replayBTN setImage:replayBTNImage forState:UIControlStateNormal];
    [_replayBTN setImage:[UIImage imageNamed:@"buttonRed"] forState:UIControlStateSelected];
    [_replayBTN addTarget:self action:@selector(replayGame:) forControlEvents:UIControlEventTouchUpInside];
    [_replayBTN setFrame:CGRectMake(screenWidth / 2 - replayBTNImage.size.width / 2, screenHeight -  CGRectGetMaxY(_endGameTitle.frame) + replayBTNImage.size.height, replayBTNImage.size.width, replayBTNImage.size.height)];
}

-(id)initWithSize:(CGSize)size {
    if (self = [super initWithSize:size]) {
        // init physics world
        self.physicsWorld.gravity = CGVectorMake(0.0, 0.0);
        self.physicsWorld.contactDelegate = self;
        
        // init sizes
        screenRect = [[UIScreen mainScreen] bounds];
        screenHeight = screenRect.size.height;
        screenWidth = screenRect.size.width;
        
        // init values of world properties
        _enemyPlaneSpeed = -1000.0;
        _bulletFiringSpeed = 0.3;
        
        _lifeNumber = LIFENUMBER;
        _lastLifeNumber = LIFENUMBER;
        _scoreNumber = 0;
        _lastScoreNumber = 0;
        _bossLifeNumber = BOSSLIFENUMBER;
        _enemyDiedForCreateBoss = 0;
        
        // adding the planes
        SKTextureAtlas *planeTextureAtlas = [SKTextureAtlas atlasNamed:@"Planes"];
        _plane = [SKSpriteNode spriteNodeWithTexture:[planeTextureAtlas textureNamed:@"playerShip1_red"]];
        _plane.scale = 0.6;
        _plane.zPosition = 2;
        _plane.position = CGPointMake(screenWidth / 2, 5 + _plane.size.height / 2 + 10);
        
        _plane.physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:_plane.size];
        _plane.physicsBody.dynamic = YES;
        _plane.physicsBody.categoryBitMask = planeCategory;
        _plane.physicsBody.contactTestBitMask = enemyCategory;
        _plane.physicsBody.collisionBitMask = 0;
        [self addChild:_plane];
        
        // adding the background
        for (int i = 0; i < 3; i++) {
            SKSpriteNode * background = [SKSpriteNode spriteNodeWithImageNamed:@"purple"];
            background.anchorPoint = CGPointZero;
            background.position = CGPointMake(0, i * background.size.height);
            background.name = @"background";
            [self addChild:background];
        }
        
        // adding smoke trail
        NSString *smokePath = [[NSBundle mainBundle] pathForResource:@"Trail" ofType:@"sks"];
        _smokeTrail = [NSKeyedUnarchiver unarchiveObjectWithFile:smokePath];
        _smokeTrail.position = CGPointMake(screenWidth / 2, 15);
        [self addChild:_smokeTrail];
        
        // core motion
        self.motionManager = [[CMMotionManager alloc] init];
        self.motionManager.accelerometerUpdateInterval = 0.2;
        [self.motionManager startAccelerometerUpdatesToQueue:[NSOperationQueue currentQueue] withHandler:^(CMAccelerometerData *accelerometerData, NSError *error) {
            [self outputAccelertionData:accelerometerData.acceleration];
            if (error) {
                NSLog(@"Core Motion Error: %@", error);
            }
        }];
        
        // scheduling enemies
        SKAction *waitForEnemies = [SKAction waitForDuration:GAMESPEED];
        SKAction *callForEnemies = [SKAction runBlock:^{
            [self generateEnemiesAndMeteors];
        }];
        SKAction *updateEnemies = [SKAction sequence:@[waitForEnemies, callForEnemies]];
        [self runAction:[SKAction repeatActionForever:updateEnemies]];

        // init boss abilities
        _isBossAppeared = NO;
        _timeOfLastBossMove = 0.0;
        _timeOfPerBossMove = 1.0;
        
        // setup HUI
        [self setupHUI];
    }
    return self;
}

#pragma mark - Accelerometer Helper
-(void)outputAccelertionData:(CMAcceleration)acceleration
{
    currentMaxAccelX = 0;
    currentMaxAccelY = 0;
    
    if (fabs(acceleration.x) > fabs(currentMaxAccelX)) {
        currentMaxAccelX = acceleration.x;
    }
    if (fabs(acceleration.y) > fabs(currentMaxAccelY)) {
        currentMaxAccelY = acceleration.y;
    }
}

#pragma mark - Generating Enemies, Boss and Meteors Method
-(void)generateEnemiesAndMeteors {
    if (!_isBossAppeared) {
        [self generateEnemies];
    }
    [self generateMeteors];
    [self generatePowerUp];
}
-(void)generatePowerUp {
    SKTextureAtlas *powerUpTextureAtlas = [SKTextureAtlas atlasNamed:@"Power-ups"];
    // generating random shield
    int randomShield = [self getRandomNumberBetween:0 to:1];
    if (randomShield == 1) {
        SKSpriteNode *shield = [SKSpriteNode spriteNodeWithTexture:[powerUpTextureAtlas textureNamed:@"shield_silver"]];
        int randomXAxis = [self getRandomNumberBetween:0 to:screenWidth];
        shield.scale = 1;
        shield.position = CGPointMake(randomXAxis, screenHeight + shield.size.height);
        shield.zPosition = 1;
        
        shield.physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:shield.size];
        shield.physicsBody.dynamic = YES;
        shield.physicsBody.categoryBitMask = powerUp_Shield;
        shield.physicsBody.contactTestBitMask = planeCategory;
        shield.physicsBody.collisionBitMask = 0;
        
        int randomTimeShield = [self getRandomNumberBetween:5 to:12];
        SKAction *moveShield = [SKAction moveTo:CGPointMake(randomXAxis, 0) duration:randomTimeShield];
        SKAction *remove = [SKAction removeFromParent];
        [shield runAction:[SKAction sequence:@[moveShield, remove]]];
        [self addChild:shield];
    }
}
-(void)generateMeteors {
    // generating random meteors
    int randomMeteor = [self getRandomNumberBetween:0 to:1];
    if (randomMeteor == 1) {
        int whichMeteor = [self getRandomNumberBetween:0 to:9];
        SKSpriteNode *meteor = [SKSpriteNode spriteNodeWithTexture:[_meteorTextures objectAtIndex:whichMeteor]];
        int randomXAxis = [self getRandomNumberBetween:0 to:screenWidth];
        meteor.scale = 0.6;
        meteor.position = CGPointMake(randomXAxis, screenHeight + meteor.size.height);
        meteor.zPosition = 1;
        int randomTimeMeteor = [self getRandomNumberBetween:9 to:15];
        SKAction *moveMeteor = [SKAction moveTo:CGPointMake(randomXAxis, 0) duration:randomTimeMeteor];
        SKAction *remove = [SKAction removeFromParent];
        [meteor runAction:[SKAction sequence:@[moveMeteor, remove]]];
        [self addChild:meteor];
    }
}
-(void)generateEnemies {
    // Random to Born
    int decisionNumber = [self getRandomNumberBetween:0 to:1];
    
    if (decisionNumber == 1) {
        SKSpriteNode *enemy;
        int whichEnemyPlane = [self getRandomNumberBetween:0 to:4];
        enemy = [self makeEnemyOfType:whichEnemyPlane];
        
        enemy.scale = 0.6;
        enemy.position = CGPointMake(screenWidth / 2, screenHeight / 2);
        enemy.zPosition = 1;
        
        enemy.physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:enemy.size];
        enemy.physicsBody.dynamic = YES;
        enemy.physicsBody.categoryBitMask = enemyCategory;
        enemy.physicsBody.contactTestBitMask = bulletCategory | planeCategory | boost_Shield;
        enemy.physicsBody.collisionBitMask = 0;
        
        CGMutablePathRef cgPath = CGPathCreateMutable();
        // Random Values
        float xStart = [self getRandomNumberBetween:0 + enemy.size.width to:screenWidth - enemy.size.width];
        float xEnd = [self getRandomNumberBetween:0 + enemy.size.width to:screenWidth - enemy.size.width];
        // ControlPoint 1
        float controlP1X = [self getRandomNumberBetween:0 + enemy.size.width to:screenWidth - enemy.size.width];
        float controlP1Y = [self getRandomNumberBetween:0 + enemy.size.width to:screenWidth - enemy.size.height];
        // ControlPoint 2
        float controlP2X = [self getRandomNumberBetween:0 + enemy.size.width to:screenWidth - enemy.size.width];
        float controlP2Y = [self getRandomNumberBetween:0 + enemy.size.width to:controlP1Y];
        CGPoint s = CGPointMake(xStart, 1024.0);
        CGPoint e = CGPointMake(xEnd, _enemyPlaneSpeed);
        CGPoint ctrP1 = CGPointMake(controlP1X, controlP1Y);
        CGPoint ctrP2 = CGPointMake(controlP2X, controlP2Y);
        CGPathMoveToPoint(cgPath, NULL, s.x, s.y);
        CGPathAddCurveToPoint(cgPath, NULL, ctrP1.x, ctrP1.y, ctrP2.x, ctrP2.y, e.x, e.y);
        
        SKAction *planeDestroy = [SKAction followPath:cgPath asOffset:NO orientToPath:YES duration:5];
        [self addChild:enemy];
        SKAction *remove = [SKAction removeFromParent];
        [enemy runAction:[SKAction sequence:@[planeDestroy, remove]]];
        
        // increasing enemy planes' speed
        _enemyPlaneSpeed = _enemyPlaneSpeed - 10;
        CGPathRelease(cgPath);
        
    }
}
-(SKSpriteNode*)makeEnemyOfType:(int)enemyType {
    SKSpriteNode *enemy;
    switch (enemyType) {
        case 0:
            enemy = [SKSpriteNode spriteNodeWithTexture:[_enemiesTextures objectAtIndex:0]];
            break;
        case 1:
            enemy = [SKSpriteNode spriteNodeWithTexture:[_enemiesTextures objectAtIndex:6]];
            break;
        case 2:
            enemy = [SKSpriteNode spriteNodeWithTexture:[_enemiesTextures objectAtIndex:12]];
            break;
        case 3:
            enemy = [SKSpriteNode spriteNodeWithTexture:[_enemiesTextures objectAtIndex:18]];
            break;
        default:
            enemy = [SKSpriteNode spriteNodeWithTexture:[_enemiesTextures objectAtIndex:19]];
            break;
    }
    return enemy;
}
-(void)generateBoss {
    _boss = [SKSpriteNode spriteNodeWithImageNamed:@"ufoYellow"];
    
    _boss.scale = 1.0;
    _boss.position = CGPointMake(screenWidth / 2, screenHeight - _boss.size.height);
    _boss.name = @"bossNode";
    _boss.zPosition = 1;
    
    _boss.physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:_boss.size];
    _boss.physicsBody.dynamic = YES;
    _boss.physicsBody.categoryBitMask = bossCategory;
    _boss.physicsBody.contactTestBitMask = bulletCategory;
    _boss.physicsBody.collisionBitMask = 0;
    [self addChild:_boss];
}
-(void)moveBossForUpdate:(NSTimeInterval)currentTime {
    if (currentTime - _timeOfLastBossMove < _timeOfPerBossMove) {
        return;
    }
    
    [self enumerateChildNodesWithName:@"bossNode" usingBlock:^(SKNode *node, BOOL *stop) {
        switch (_bossMovementDirection) {
            case BossMovementDirectionLeft:
                node.position = CGPointMake(node.position.x - 20, node.position.y);
                break;
            case BossMovementDirectionRight:
                node.position = CGPointMake(node.position.x + 20, node.position.y);
                break;
            case BossMovementDirectionDownThenLeft:
                node.position = CGPointMake(node.position.x, node.position.y - 10);
                node.position = CGPointMake(node.position.x - 20, node.position.y);
                break;
            case BossMovementDirectionDownThenRight:
                node.position = CGPointMake(node.position.x, node.position.y - 10);
                node.position = CGPointMake(node.position.x + 20, node.position.y);
                break;
            case BossMovementDirectionNone:
                break;
            default:
                break;
        }
    }];
    // updating time of last boss move
    _timeOfLastBossMove = currentTime;
    // updating movement direction
    [self determineBossMovementDirection];
}
-(void)determineBossMovementDirection {
    __block BossMovementDirection proposedMovementDirection = _bossMovementDirection;
    
    [self enumerateChildNodesWithName:@"bossNode" usingBlock:^(SKNode *node, BOOL *stop) {
        switch (_bossMovementDirection) {
            case BossMovementDirectionLeft:
                if (CGRectGetMinY(node.frame) >= screenHeight - 300) {
                    if (CGRectGetMinX(node.frame) <= 1.0f) {
                        proposedMovementDirection = BossMovementDirectionDownThenRight;
                        *stop = YES;
                    }
                } else if (CGRectGetMinX(node.frame) <= 1.0f) {
                    proposedMovementDirection = BossMovementDirectionRight;
                    *stop = YES;
                } else {
                    proposedMovementDirection = BossMovementDirectionLeft;
                    *stop = YES;
                }

                break;
            case BossMovementDirectionRight:
                if (CGRectGetMinY(node.frame) >= screenHeight - 300) {
                    if (CGRectGetMaxX(node.frame) >= node.scene.size.width - 1.0) {
                        proposedMovementDirection = BossMovementDirectionDownThenLeft;
                        *stop = YES;
                    }
                } else if (CGRectGetMaxX(node.frame) >= node.scene.size.width - 1.0) {
                    proposedMovementDirection = BossMovementDirectionLeft;
                    *stop = YES;
                } else {
                    proposedMovementDirection = BossMovementDirectionRight;
                    *stop = YES;
                }
                break;
            case BossMovementDirectionDownThenLeft:
                proposedMovementDirection = BossMovementDirectionLeft;
                *stop = YES;
                break;
            case BossMovementDirectionDownThenRight:
                proposedMovementDirection = BossMovementDirectionRight;
                *stop = YES;
                break;
            default:
                break;
        }
    }];
    
    if (proposedMovementDirection != _bossMovementDirection) {
        _bossMovementDirection = proposedMovementDirection;
    }
}

#pragma mark - HUI Helpers
-(void)setupHUI {
    // displaying the highscore
    [self displayScore:_scoreNumber];
    // displaying the plane's lives
    [self displayLife:_lifeNumber];
    _isHUICreated = YES;
}
-(void)displayScore:(int)score {
    NSMutableArray *scoreDigitsArray;
    scoreDigitsArray = [self convertIntToCharacters:score];
    int count = 0;
    for (NSNumber *digit in scoreDigitsArray) {
        count++;
        SKSpriteNode *scoreDigitNode = [SKSpriteNode spriteNodeWithImageNamed:[NSString stringWithFormat:@"numeral%@",digit]];
        scoreDigitNode.zPosition = 2;
        scoreDigitNode.scale = 0.8;
        scoreDigitNode.name = @"scoreDigitNodeLevel";
        scoreDigitNode.position = CGPointMake(2 + scoreDigitNode.size.width * count, screenHeight - TOP_SCREEN_INSET);
        [self addChild:scoreDigitNode];
    }
}
-(void)updateNewScore:(int)newScore {
    // removing old score
    [self enumerateChildNodesWithName:@"scoreDigitNodeLevel" usingBlock:^(SKNode *node, BOOL *stop) {
        [node removeFromParent];
    }];
    // inserting new score
    [self displayScore:newScore];
}
-(void)displayLife:(int)numberOfLive {
    
    SKSpriteNode *lifeNumberNode = [SKSpriteNode spriteNodeWithImageNamed:[NSString stringWithFormat:@"numeral%d",numberOfLive]];
    lifeNumberNode.zPosition = 2;
    lifeNumberNode.scale = 0.8;
    lifeNumberNode.name = @"lifeNumberNode";
    lifeNumberNode.position = CGPointMake(screenWidth - lifeNumberNode.size.width - 5, screenHeight - TOP_SCREEN_INSET);
    [self addChild:lifeNumberNode];
    
    if (!_isHUICreated) {
        SKSpriteNode *xMarkNode = [SKSpriteNode spriteNodeWithImageNamed:@"numeralX"];
        xMarkNode.zPosition = 2;
        xMarkNode.scale = 0.8;
        xMarkNode.position = CGPointMake(lifeNumberNode.position.x - xMarkNode.size.width - 5, screenHeight - TOP_SCREEN_INSET);
        [self addChild:xMarkNode];
        
        SKSpriteNode *lifeNode = [SKSpriteNode spriteNodeWithImageNamed:@"playerLife1_red"];
        lifeNode.zPosition = 2;
        lifeNode.scale = 0.8;
        lifeNode.position = CGPointMake(xMarkNode.position.x - lifeNode.size.width - 5, screenHeight - TOP_SCREEN_INSET);
        [self addChild:lifeNode];
    }

}
-(void)updateNewLife:(int)newLife {
    if (newLife >= 0) {
        // removing old score
        [self enumerateChildNodesWithName:@"lifeNumberNode" usingBlock:^(SKNode *node, BOOL *stop) {
            [node removeFromParent];
        }];
        // inserting new score
        [self displayLife:newLife];
    }
}

#pragma mark - Other Helpers
-(int)getRandomNumberBetween:(int)from to:(int)to {
    return (int)from + arc4random() % (to - from + 1);
}

-(NSMutableArray*)convertIntToCharacters:(int)number {
    NSString *numberString = [[NSNumber numberWithInt:number] stringValue];
    NSMutableArray *digitArray = [NSMutableArray array];
    
    for (int i = 0; i < numberString.length; i++) {
        unichar c = [numberString characterAtIndex:i];
        
        if (isalnum(c)) {
            [digitArray addObject:[NSNumber numberWithInt:c - '0']];
        } else {
            [digitArray addObject:[NSNumber numberWithInt:-1]];
        }
    }
    return digitArray;
}

#pragma mark - User Interaction Helpers
-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    /* Called when a touch begins */
    [self firingBullet];
}

#pragma mark - Firing Bullet Helper
-(void)firingBullet
{
    if (!_isGameOver) {
        // Create Bullets
        CGPoint location = [_plane position];
        SKSpriteNode *bullet;
        int bulletSpacing = [self getRandomNumberBetween:-(_plane.size.width / 3) to:_plane.size.width / 3];
        
        if (bulletSpacing % 2 == 0) {
            bullet = [SKSpriteNode spriteNodeWithImageNamed:@"laserGreen10"];
        } else {
            bullet = [SKSpriteNode spriteNodeWithImageNamed:@"laserGreen11"];
        }
        bullet.position = CGPointMake(location.x + bulletSpacing, location.y + _plane.size.height / 2);
        bullet.zPosition = 1;
        bullet.scale = 0.6;
        
        bullet.physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:bullet.size];
        bullet.physicsBody.dynamic = NO;
        bullet.physicsBody.categoryBitMask = bulletCategory;
        bullet.physicsBody.contactTestBitMask = enemyCategory;
        bullet.physicsBody.collisionBitMask = 0;
        
        SKAction *fireBullet = [SKAction moveToY:self.frame.size.height + bullet.size.height duration:2];
        SKAction *playSound = [SKAction playSoundFileNamed:@"laser1.mp3" waitForCompletion:NO];
        SKAction *remove = [SKAction removeFromParent];
        [bullet runAction:[SKAction sequence:@[fireBullet, playSound, remove]]];
        [self addChild:bullet];
    }
}
-(void)bossFiring:(SKSpriteNode*)boss {
    SKSpriteNode *bullet = [SKSpriteNode spriteNodeWithImageNamed:@"laserGreen14"];
    bullet.position = CGPointMake(CGRectGetMidX(boss.frame), CGRectGetMinY(boss.frame));
    bullet.zPosition = 1;
    bullet.scale = 0.6;
    
    bullet.physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:bullet.size];
    bullet.physicsBody.dynamic = NO;
    bullet.physicsBody.categoryBitMask = bossBulletCategory;
    bullet.physicsBody.contactTestBitMask = planeCategory;
    bullet.physicsBody.collisionBitMask = 0;
    
    SKAction *fireBullet = [SKAction moveTo:CGPointMake(CGRectGetMidX(_plane.frame), CGRectGetMaxY(_plane.frame)) duration:1];
    
    SKAction *remove = [SKAction removeFromParent];
    [bullet runAction:[SKAction sequence:@[fireBullet, remove]]];
    [self addChild:bullet];
}
- (void)updateWithTimeSinceLastUpdate:(CFTimeInterval)timeSinceLast {
    
}

#pragma mark - Detect Physical Contacts
-(void)didBeginContact:(SKPhysicsContact *)contact {
    SKPhysicsBody *firstBody;
    SKPhysicsBody *secondBody;
    
    if (contact.bodyA.categoryBitMask < contact.bodyB.categoryBitMask) {
        firstBody = contact.bodyA;
        secondBody = contact.bodyB;
    } else {
        firstBody = contact.bodyB;
        secondBody = contact.bodyA;
    }
    
    // handling physic contact between PLANE and ENEMY
    if (!_isBoostedWithShield) {
        if (((firstBody.contactTestBitMask & enemyCategory) == 2) && (secondBody.categoryBitMask == planeCategory)) {
            SKNode *enemy = (contact.bodyA.categoryBitMask & enemyCategory) ? contact.bodyB.node : contact.bodyA.node;
            [enemy runAction:[SKAction removeFromParent]];
            [self displayExplosion:enemy.position];
            [self collidedWith:enemy.physicsBody];
        }
    }
    
    // handling physic contact between BULLET and ENEMY
    if (((firstBody.categoryBitMask & bulletCategory) == 1) && (secondBody.categoryBitMask == enemyCategory)) {
        SKNode *projectile = (contact.bodyA.categoryBitMask & bulletCategory) ? contact.bodyA.node : contact.bodyB.node;
        SKNode *enemy = (contact.bodyA.categoryBitMask & bulletCategory) ? contact.bodyB.node : contact.bodyA.node;
        [projectile runAction:[SKAction removeFromParent]];
        [self applyDamage:enemy];
    }
    
    // handling physic contact between BULLET and BOSS
    if (((firstBody.categoryBitMask & bulletCategory) == 1) && (secondBody.categoryBitMask == bossCategory)) {
        SKNode *projectile = (contact.bodyA.categoryBitMask == bulletCategory) ? contact.bodyA.node : contact.bodyB.node;
        SKNode *boss = (contact.bodyA.categoryBitMask == bossCategory) ? contact.bodyA.node : contact.bodyB.node;
        [projectile runAction:[SKAction removeFromParent]];
        [self applyDamage:boss];
    }
    
    // handling physic contact between BOSSBULLET and PLANE
    if (((firstBody.categoryBitMask & planeCategory) == 3) && (secondBody.categoryBitMask == bossBulletCategory)) {
        SKNode *projectile = (contact.bodyA.categoryBitMask & bossBulletCategory) ? contact.bodyA.node : contact.bodyB.node;
        SKNode *plane = (contact.bodyA.categoryBitMask == planeCategory) ? contact.bodyA.node : contact.bodyB.node;
        [projectile runAction:[SKAction removeFromParent]];
        [self collidedWith:plane.physicsBody];
    }
    
    // handling physic contact between POWERUP_SHIELD and PLANE
    if (!_isBoostedWithShield) {
        if (((firstBody.categoryBitMask & planeCategory) == 3) && (secondBody.categoryBitMask == powerUp_Shield)) {
            SKNode *shield = (contact.bodyA.categoryBitMask == powerUp_Shield) ? contact.bodyA.node : contact.bodyB.node;
            [shield runAction:[SKAction removeFromParent]];
            [self applyEffectWithPowerUp:shield];
        }
    }
    
    // handling physic contact between SHIELD and OTHERS (BULLETS & ENEMIES)
    if ((((firstBody.categoryBitMask & planeCategory) != 3) || ((firstBody.categoryBitMask & powerUp_Shield) != 6)) && (secondBody.categoryBitMask == boost_Shield))  {
        SKNode *shield = (contact.bodyA.categoryBitMask & boost_Shield) ? contact.bodyA.node : contact.bodyB.node;
        SKNode *other = (contact.bodyA.categoryBitMask == planeCategory) ? contact.bodyA.node : contact.bodyB.node;
        [other runAction:[SKAction removeFromParent]];

    }
}
-(void)applyDamage:(SKNode*)object {

    if ((object.physicsBody.categoryBitMask & enemyCategory) == 2) {
        [object runAction:[SKAction removeFromParent]];
        [self displayExplosion:object.position];
        // updating score
        _scoreNumber++;
        // check to generate boss
        if (_enemyDiedForCreateBoss < 9) {
            _enemyDiedForCreateBoss++;
        } else if (!_isBossAppeared){
            [self generateBoss];
            _isBossAppeared = YES;
            _enemyDiedForCreateBoss = 0;
        }
        // increasing Enemy Plane Speed
        _enemyPlaneSpeed -= 400;
    }
    
    if ((object.physicsBody.categoryBitMask & bossCategory) == 5) {
        if (_bossLifeNumber > 0) {
            _bossLifeNumber--;
        }
        if (_bossLifeNumber == 0) {
            [object runAction:[SKAction removeFromParent]];
            [self displayExplosion:object.position];
            _scoreNumber += 20;
            _isBossAppeared = NO;
            _bossLifeNumber = BOSSLIFENUMBER;
        }
    }
}
-(void)displayExplosion:(CGPoint)explosionPosition {
    // adding explosion
    
    SKSpriteNode *explosion = [SKSpriteNode spriteNodeWithTexture:[_explosionTextures objectAtIndex:0]];
    explosion.zPosition = 1;
    explosion.scale = 0.4;
    explosion.position = explosionPosition;
    [self addChild:explosion];
    SKAction *explosionAction = [SKAction animateWithTextures:_explosionTextures timePerFrame:0.07];
    SKAction *remove = [SKAction removeFromParent];
    [explosion runAction:[SKAction sequence:@[explosionAction, remove]]];
}
-(void)collidedWith:(SKPhysicsBody*)enemy {
    if (enemy.categoryBitMask & planeCategory) {
        _lifeNumber--;
        SKAction *playSound = [SKAction playSoundFileNamed:@"spaceTrash1.mp3" waitForCompletion:NO];
        [self runAction:playSound];
    }
}
-(void)applyEffectWithPowerUp:(SKNode*)powerUp {
    SKTextureAtlas *powerUpTextureAtlas = [SKTextureAtlas atlasNamed:@"Effects"];
    if (!_isBoostedWithShield) {
        if (powerUp.physicsBody.categoryBitMask == powerUp_Shield) {
            SKSpriteNode *shield = [SKSpriteNode spriteNodeWithTexture:[powerUpTextureAtlas textureNamed:@"shield1"]];
            shield.zPosition = 2;
            shield.name = @"shield";
            shield.scale = 0.8;
            shield.position = CGPointMake(_plane.position.x, _plane.position.y + 30);
            
            shield.physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:shield.size];
            shield.physicsBody.dynamic = YES;
            shield.physicsBody.categoryBitMask = boost_Shield;
            shield.physicsBody.contactTestBitMask = enemyCategory | bossBulletCategory;
            shield.physicsBody.collisionBitMask = 0;
            [self addChild:shield];
        }
        _isBoostedWithShield = YES;
    }

}

#pragma mark - Win/Lose or Level and Replay the game
-(void)checkForGameOver {
    if (_lifeNumber == 0) {
        [self gameOver:1];
    }
}
-(void)gameOver:(BOOL)lose {
    self.isGameOver = YES;
    [self addChild:_endGameTitle];
    [self.view addSubview:_replayBTN];
}
-(void)replayGame:(id)sender {
    // remove "replay" button
    [[self.view viewWithTag:999] removeFromSuperview];
    // representing the game
    [self.view presentScene:[[sunMyScene alloc] initWithSize:self.size]];
}

#pragma mark - Update
-(void)update:(CFTimeInterval)currentTime {
    /* Called before each frame is rendered */
    // checking whether the game is over or not
    if (_isGameOver) {
        return;
    }
    [self checkForGameOver];
    
    // calculating delta time period
    if (lastUpdateTime) {
        deltaTime = currentTime - lastUpdateTime;
    }
    else {
        deltaTime = 0;
    }
    lastUpdateTime = currentTime;
    
    if ( currentTime - lastMissileAdded > _bulletFiringSpeed) {
        lastMissileAdded = currentTime + _bulletFiringSpeed;
//        [self firingBullet];
        if (_isBossAppeared) {
            [self bossFiring:_boss];
        }
    }
    
    // updating GUI - Score
    if (_scoreNumber > _lastScoreNumber) {
        [self updateNewScore:_scoreNumber];
        _lastScoreNumber = _scoreNumber;
    }
    
    // updating GUI - Health
    if (_lifeNumber < _lastLifeNumber) {
        [self updateNewLife:_lifeNumber];
        _lastLifeNumber = _lifeNumber;
    }
    
    // moving boss
    if (_isBossAppeared) {
        [self enumerateChildNodesWithName:@"bossNode" usingBlock:^(SKNode *node, BOOL *stop) {
            [self moveBossForUpdate:currentTime];
        }];
    }
    
    // updating background
    [self enumerateChildNodesWithName:@"background" usingBlock:^(SKNode *node, BOOL *stop) {
        SKSpriteNode *background = (SKSpriteNode*) node;
        background.position = CGPointMake(background.position.x, background.position.y - 15);
        if (background.position.y <= -background.size.height) {
            background.position = CGPointMake(background.position.x, background.position.y + background.size.height * 3);
        }
    }];
    
    // updating shield position
    [self enumerateChildNodesWithName:@"shield" usingBlock:^(SKNode *node, BOOL *stop) {
        SKSpriteNode *shield = (SKSpriteNode*) node;
        shield.position = CGPointMake(_plane.position.x, _plane.position.y);
    }];
    
    // accelerate plane
    float maxY = screenWidth - _plane.size.width;
    float minY = _plane.size.width / 2;
    float newX = 0;
    newX = currentMaxAccelX * 10;
    newX = MIN(MAX(newX+_plane.position.x,minY),maxY);
    _plane.position = CGPointMake(newX, _plane.size.height);
    _smokeTrail.position = CGPointMake(_plane.position.x, _plane.position.y - _plane.size.height / 2);
}

@end

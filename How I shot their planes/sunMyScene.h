//
//  sunMyScene.h
//  How I shot their planes
//

//  Copyright (c) 2014 El Desperado. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>
#import <CoreMotion/CoreMotion.h>

#define LIFENUMBER 3;
#define BOSSLIFENUMBER 20;

static const float TOP_SCREEN_INSET = 30.0;
static const float GAMESPEED = 0.6;

static const uint8_t bulletCategory = 1;
static const uint8_t enemyCategory = 2;
static const uint8_t planeCategory = 3;
static const uint8_t bossBulletCategory = 4;
static const uint8_t bossCategory = 5;
static const uint8_t powerUp_Shield = 6;
static const uint8_t boost_Shield = 7;

typedef enum BossMovementDirection {
    BossMovementDirectionRight,
    BossMovementDirectionLeft,
    BossMovementDirectionDownThenRight,
    BossMovementDirectionDownThenLeft,
    BossMovementDirectionNone
} BossMovementDirection;

@interface sunMyScene : SKScene <UIAccelerometerDelegate, SKPhysicsContactDelegate> {
    CGRect screenRect;
    CGFloat screenHeight;
    CGFloat screenWidth;
    double currentMaxAccelX;
    double currentMaxAccelY;
    NSTimeInterval lastUpdateTime;
    NSTimeInterval deltaTime;
    NSTimeInterval lastMissileAdded;
}

@property (nonatomic, strong) CMMotionManager       *motionManager;
@property SKSpriteNode *plane;
@property SKSpriteNode *boss;
@property SKEmitterNode *smokeTrail;
@property NSMutableArray *explosionTextures;
@property NSMutableArray *enemiesTextures;
@property NSMutableArray *meteorTextures;
@property BOOL isContentLoaded;
@property BOOL isHUICreated;
@property BossMovementDirection bossMovementDirection;
@property NSTimeInterval timeOfLastBossMove;
@property NSTimeInterval timeOfPerBossMove;
@property BOOL isBossAppeared;
@property BOOL isBoostedWithShield;
@property BOOL isGameOver;
@property SKLabelNode *endGameTitle;
@property UIButton *replayBTN;

@property float enemyPlaneSpeed;
@property float bulletFiringSpeed;

@property int lifeNumber;
@property int lastLifeNumber;
@property int scoreNumber;
@property int lastScoreNumber;
@property int bossLifeNumber;
@property int enemyDiedForCreateBoss;

@end

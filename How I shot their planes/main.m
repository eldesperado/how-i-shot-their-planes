//
//  main.m
//  How I shot their planes
//
//  Created by El Desperado on 2/4/14.
//  Copyright (c) 2014 El Desperado. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "sunAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([sunAppDelegate class]));
    }
}
